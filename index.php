<html>
    <head>
        <title>Blackjack calculation</title>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/style.css">

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Blackjack calculation</h2>
                    <p>Please enter two cards the first part representing the face value from 2-10, plus A, K, Q, J. 
                        The second part represents the suit S, C, D, H. without spaces. Eg "8H" for heart 8, "JC" for club Jack.</p>
                    <form class="form-inline" role="form">
                        <div class="form-group col-lg-3">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    Card 1
                                </div>
                                <input class="form-control" name="card_1" id="card_1" placeholder="Enter Card 1">
                            </div>
                        </div>

                        <div class="form-group col-lg-3">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    Card 2
                                </div>
                                <input class="form-control" name="card_2" id="card_2" placeholder="Enter Card 2">
                            </div>
                        </div>

                        <button type="button" class="btn btn-default" id="submit">
                            Submit
                        </button>
                    </form>
                </div>
            </div>
            <hr />
            <div class="row" id="result_div">

            </div>
        </div>
    <script>
        $(function(){
            $("#submit").click(function(){
                var card_value_1 = $("#card_1").val();
                var card_value_2 = $("#card_2").val();
                $.get( "result.php?card_1="+card_value_1+"&card_2="+card_value_2, function( data ) {
                    $("#result_div").html(data);
                });
            })
        });
    </script>
    </body>
</html>