<?php
/**
 * Cards.php 
 * It is a class to store cards in hand and calculate the total score of all cards. 
 * The purpose of this project is to calculate 2 cards value though this class is designed to handle any number of cards.
 * 
 * @author     Rui Duan <ruiduan15@gmail.com>
 * @date       17/08/2014
 **/
 
require_once dirname(__FILE__) . '/Card.php';
class Cards {
    protected $allCards;
    protected $allCardsValue;
    protected $cardBust;
    protected $numberOfA;
    
    public function __construct(){
        $this->allCards = array();
        $this->allCardsValue = 0;
        $this->cardBust = false;
        $this->numberOfA = 0;
    }
    /*
     * Function calculate
     * Calculate the total value of all the cards in hand - can be used for more than 2 cards
     * Set the property value for allCardsValue & cardBust
     */
    public function calculate() {
        foreach($this->allCards as $cardToCalculate){
            $card = new Card($cardToCalculate);
            $cardValue = $card->getValue();
            $this->allCardsValue += $cardValue;
            
            //Check if it is soft hand with A
            if($card->getFace() == 'A'){
                $this->numberOfA ++;
            }
            
            //Set bust if the score is more than 21 with soft hand checking
            if($this->allCardsValue > 21){
                if($this->numberOfA > 0){
                    $this->allCardsValue -= 10;
                    $this->numberOfA --;
                }
                else{
                    $this->cardBust = true;
                    $this->allCardsValue = 0;
                    break;
                }
            }
        }
    }
    
    /*
     * Function placeCard
     * Add one card to allCards array
     * 
     * @param cardToPlace is an input string from user
     * @return error message if the card is not valid or no error
     */
    public function placeCard($cardToPlace){
        $cardToPlace = strtoupper($cardToPlace);
        $card = new Card($cardToPlace);
        if(!$card->getFace() || !$card->getSuit()){
            return "invalid_card";
        }
        if(in_array($cardToPlace, $this->allCards)){
            return "used_card";
        }
        $this->allCards[] = $cardToPlace;
        return "no_error";
    }
    
    
    /*
     * Function getCardsValue
     * Get all cards in hand value
     * @return protected member allCardsValue
     */
    public function getCardsValue(){
        return $this->allCardsValue;
    }
}