This project is a simple calculation for Blackjack score calculation.

There are 2 folders: css, test; and 4 main files: Card.php, Cards.php, index.php, result.php

Folder css: the style.css contains basic style and import font.

Folder test: there are two unit test class files: CardsTest.php and CardTest.php. 
This project is using the TDD (test driven development) method. All production classes and functions are written after test case created. 
The unit test framewok is PHPUnit. It requires PHPUnit to test.

Card.php: it is a class for single card. Public functions getFace, getSuit, getValue can be used to retrieve the card information by user enter string (the handed card).

Cards.php: it is a class to store cards in hand and calculate the total score of all cards. The purpose of this project is to calculate 2 cards value though this class is designed to handle any number of cards.

index.php: it is the test interface for user input. The result is using AJAX to populate the result as an one page application.

result.php: the calculation part loaded from AJAX call of index.php

To test the project:
1. Upload all files to your web directory. Eg. www_root\blackjack\
2. Open browser then navigate to the index.php.
3. Enter 2 cards value and click submit.

Please note, this project needs jQuery and Bootstrap packages. It is linking to the CND of those packages hence you need Internet connection to test it.
 


