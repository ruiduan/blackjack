<?php
/**
 * Card.php 
 * It is a class for single card. Public functions getFace, getSuit, getValue can be used to retrieve the card 
 * information by user enter string (the handed card).
 * @author     Rui Duan <ruiduan15@gmail.com>
 * @date       17/08/2014
 **/
class Card {
    protected $handedCard;
    protected $suit;
    protected $face;
    protected $value;
    
    public function __construct($handedCard = null) {
        $this->handedCard = strtoupper($handedCard);;
    }
    
    /*
     * Function getFace
     * Get card face from input card.
     * 
     * @param handedCard is an input string from user
     * @return false if the card face is not valid otherwise return the face value
     */
    public function getFace($handedCard = null){
        if(!isset($handedCard)){
            $handedCard = $this->handedCard;
        }
        if(strlen($handedCard) == 0){
            return false;
        }
        $handedCard = strtoupper($handedCard);
        
        //get the face string with input string minus the last character
        $face = substr($handedCard, 0, strlen($handedCard)- 1);
        
        
        //check if face is valid
        $faces = array(2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K', 'A');
        if(strlen($face) == 0 || strlen($face) > 2 || !in_array(strtoupper($face), $faces)){
            return false;
        }
        
        //set local member value and return face value
        $this->face = $face;
        return $this->face;
    }    
    
    /*
     * Function getSuit
     * Get card suit from input card
     * 
     * @param handedCard is an input string from user
     * @return false if the card suit is not valid or the suit value
     */
    public function getSuit($handedCard = null){
        if(!isset($handedCard)){
            $handedCard = $this->handedCard;
        }
        $handedCard = strtoupper($handedCard);
        if(strlen($handedCard) == 0){
            return false;
        }
        
        //get the last character of input string as suit
        $suit = substr($handedCard, -1,1);
        
        
        //check if suit is valid
        $suits = array('S', 'C', 'D', 'H');
        if(!in_array(strtoupper($suit), $suits)){
            return false;
        }
        
        //set local member value and return suit value
        $this->suit = $suit;
        return $this->suit;
    }
     
    /*
     * Function getValue
     * Get card value from input card
     * 
     * @param handedCard is an input string from user
     * @return (int) card value
     */
    public function getValue($handedCard = null){
        if(!isset($handedCard)){
            $handedCard = $this->handedCard;
        }
        $face = $this->getFace($handedCard);
        
        //calculate face value
        switch ($face) {
            case 'A' :
            $value = 11;
            break;
            case 'J':
            case 'Q':
            case 'K':
            $value = 10;
            break;
            default:
            $value = $face;
            break;
        }
        
        //check if face value is valid and set local member value
        if(is_numeric($value)){
            $this->value = $value;
        }
        else{
            $this->value = 0;
        }
        
        //return face value
        return $this->value;
    }
}