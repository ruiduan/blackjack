<?php

require_once dirname(__FILE__) . '/../Card.php';
 
class CardTest extends PHPUnit_Framework_TestCase {
 
    private $card;
 
    function setUp() {
        $this->card = new Card();
    }
 
    function testEmptyCardFace() {
        $this->assertFalse($this->card->getFace(''));
    }
     
    function testInvalidCardFaceNumberTooSmall() {
        $handedCard = '1';
        $this->assertFalse($this->card->getFace($handedCard));
    }
    
    function testInvalidCardFaceNumberTooLarge() {
        $handedCard = '11';
        $this->assertFalse($this->card->getFace($handedCard));
    }
    
    function testInvalidCardFaceLetter() {
        $handedCard = 'B';
        $this->assertFalse($this->card->getFace($handedCard));
    }
    
    function testInvalidCardInvalidSuitLetter() {
        $handedCard = 'B';
        $this->assertFalse($this->card->getSuit($handedCard));
    }
    
    function testEmptyCardSuit() {
        $this->assertFalse($this->card->getSuit(''));
    }
    
    function testGetOneCardValueWithNumber() {
        $cardToCalculate = '8D';
        $this->assertEquals('8',$this->card->getValue($cardToCalculate));
    }
    
    function testGetOneCardValueWithLetter() {
        $cardToCalculate = 'JH';
        $this->assertEquals('10',$this->card->getValue($cardToCalculate));
    }
}