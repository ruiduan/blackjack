<?php

require_once dirname(__FILE__) . '/../Cards.php';
 
class CardsTest extends PHPUnit_Framework_TestCase {
 
    private $cards;
 
    function setUp() {
        $this->cards = new Cards();
    }

    function testPlaceExistingCard(){
        $card1 = $card2 = "7D";
        $this->cards->placeCard($card1);
        $this->assertEquals('used_card', $this->cards->placeCard($card2));
    }
    
    function testPlaceInvalidCard(){
        $card = "BD";
        $this->assertEquals('invalid_card', $this->cards->placeCard($card));
    }
    
    function testGetCardsValue(){
        $cardToCalculate1 = '3D';
        $cardToCalculate2 = 'QH';
        $cardToCalculate2 = '7S';
        
        $this->cards->placeCard($cardToCalculate1);
        $this->cards->placeCard($cardToCalculate2);
        $this->cards->placeCard($cardToCalculate3);
        $this->cards->calculate();
        $this->assertEquals(20, $this->cards->getCardsValue());
    }
}