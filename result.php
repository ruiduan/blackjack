<?php

require_once dirname(__FILE__) . '/Card.php';
require_once dirname(__FILE__) . '/Cards.php';

//define the error message
$error_code['invalid_card'] = "is invalid";
$error_code['used_card'] = "has been used";

//get the two cards input string
$cardToCalculate1 = trim($_GET['card_1']);
$cardToCalculate2 = trim($_GET['card_2']);

//place the input string to cards object
$cards = new Cards();
$placeResult = $cards->placeCard($cardToCalculate1);

//validate the input cards
$message = "";
if($placeResult !== 'no_error'){
    $message = "Card 1 ".$error_code[$placeResult].". ";
}
$placeResult = $cards->placeCard($cardToCalculate2);
if($placeResult !== 'no_error'){
    $message .= "Card 2 ".$error_code[$placeResult].". ";
}

//do the calculation if no error
if($message == ""){
    $cards->calculate();
    $cardsValue = $cards->getCardsValue();
    if($cardsValue == 21){
        $message = "Congratulations! ";
    }
    $message .= "Your score is <strong>".$cardsValue."</strong>";
}
?>
<div>
    <button type="button" class="btn btn-primary btn-lg"><?=$message?></button>
</div>